import { Component, inject, Renderer2 } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavComponent } from '@examples/ui/navigation';
import { DOCUMENT } from '@angular/common';

@Component({
  standalone: true,
  imports: [RouterModule, NavComponent],
  selector: 'examples-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'mat-examples';
  private doc = inject(DOCUMENT);
  private renderer = inject(Renderer2);


  onThemeChange(darkTheme: boolean) {
    const theme: string = darkTheme ? 'dark-theme' : 'light-theme';
    this.renderer.setAttribute(this.doc.body, 'class', theme);
  }
}
